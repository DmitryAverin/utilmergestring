import java.io.File

fun main() {
    println("Hello World!")

    val patternClosingSymbol = "\">"


    File("src/main/kotlin/merged/strings.xml").bufferedWriter().use { mergedFileWriter ->
        File("src/main/kotlin/old/strings.xml").forEachLine { line ->
            var lineToWrite = line.trim()
            val indexClosingSymbol = lineToWrite.indexOf(patternClosingSymbol)
            if (indexClosingSymbol != -1) {
                val substringSearch = lineToWrite.substring(0, indexClosingSymbol + patternClosingSymbol.length)
                searchInNewFile(substringSearch)?.let { foundString ->
                    if (lineToWrite != foundString) {
                        println("Замена строки \n${lineToWrite}\n на \n${foundString} \n\n ")
                        lineToWrite = foundString
                    }
                }
            }
            mergedFileWriter.write(lineToWrite + "\n")
        }
    }
}

fun searchInNewFile(pattern: String): String? {
    val handledLines = mutableListOf<String>()
    val readLines = File("src/main/kotlin/new/strings.xml").readLines()

    var buffer: String? = null // когда ресурс имеет несколько строк xml
    readLines.forEachIndexed { index, line ->
        line.trim().let { mLine ->
            when {
                // ресурсы и прочее
                mLine.isEmpty() || mLine.startsWith("<r") || mLine.startsWith("</r") -> handledLines.add(mLine)

                //обычные строки
                mLine.startsWith("<string>") && mLine.endsWith("</string>") -> {
                    buffer?.plus("\\n")?.let {
                        handledLines.add(it + mLine)
                    } ?: handledLines.add(mLine)

                    buffer = null
                }

                mLine.endsWith("</string>") -> {
                    buffer?.plus("\\n")?.let {
                        handledLines.add(it + mLine)
                    } ?: handledLines.add(mLine)

                    buffer = null
                }

                //комментарии (по крайней мере только типа // )
                mLine.startsWith("//") -> {
                    handledLines.add(index, mLine)
                }
                // обработка случая, когда локалйз разделил на несколько строк один ресурс
                else -> {
                    buffer = if (buffer.isNullOrEmpty()) {
                        mLine
                    } else {
                        "$buffer\\n$mLine"
                    }
                }
            }
        }
    }
    return handledLines.find { it.contains(pattern) }
}